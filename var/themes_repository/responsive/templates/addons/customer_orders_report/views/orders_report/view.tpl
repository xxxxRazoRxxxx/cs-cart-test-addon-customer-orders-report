{capture name="section"}
    {include file="addons/customer_orders_report/views/orders_report/components/report_params.tpl"}
{/capture}
{include file="common/section.tpl" section_title=__("customer_orders_report.report_params") section_content=$smarty.capture.section class="ty-search-form" collapse=false}

{if $report}
    <div class="clearfix">
        <div class="ty-control-group">
            {if empty($report.data)}
                <p class="ty-no-items ty-compare__no-items">{__("text_no_orders")}</p>
            {elseif $report.type == 'general'}
                {include file="addons/customer_orders_report/views/orders_report/components/general_report.tpl"}
            {elseif $report.type == 'spent_money'}
                {include file="addons/customer_orders_report/views/orders_report/components/table_report.tpl"}
            {elseif $report.type == 'top_products'}
                {include file="addons/customer_orders_report/views/orders_report/components/top_report.tpl" data=$report.data type='products'}
            {elseif $report.type == 'top_categories'}
                {include file="addons/customer_orders_report/views/orders_report/components/top_report.tpl" data=$report.data type='categories'}
            {/if}
        </div>
    </div>
{/if}
