{if $type == 'products'}
    {$url = 'products.view&product_id='}
{else}
    {$url = 'categories.view&category_id='}
{/if}

<h3 class="ty-subheader">
    {__("customer_orders_report.`$report.type`")}
</h3>


<table class="ty-table">
    {foreach from=$data item='element'}
        <tr>
            <td><a href="{"`$url``$element.id`"|fn_url}" title="{$element.name nofilter}">{$element.name}</a></td>
            <td>{$element.count}</td>
        </tr>
    {/foreach}
</table>