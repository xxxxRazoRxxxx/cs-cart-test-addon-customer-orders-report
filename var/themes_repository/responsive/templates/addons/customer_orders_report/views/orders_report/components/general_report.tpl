{$data = $report.data}

<h3 class="ty-subheader">
    {__("customer_orders_report.paid")}
</h3>

<p>{__("customer_orders_report.paid_orders")}: {$data.paid_orders}</p>
<p>{__("customer_orders_report.paid_orders_total")}:
    {include file="common/price.tpl" value=$data.paid_orders_total}
</p>
<p>{__("customer_orders_report.paid_products")}: {$data.paid_products} </p>

<h3 class="ty-subheader">
    {__("customer_orders_report.unpaid")}
</h3>

<p>{__("customer_orders_report.unpaid_orders")}: {$data.unpaid_orders}</p>
<p>{__("customer_orders_report.unpaid_orders_total")}:
    {include file="common/price.tpl" value=$data.unpaid_orders_total}
</p>
<p>{__("customer_orders_report.unpaid_products")}: {$data.unpaid_products}</p>