<form action="{""|fn_url}" class="ty-orders-search-options" name="report_params_form" method="get">
    <div class="clearfix">
        <div class="span4 ty-control-group">
            <label class="ty-control-group__title">{__("customer_orders_report.report_type")}</label>
            <select class="input-medium" name="type" id="type">
                {foreach from=$report_types item=type key=key}
                    <option value={$type} {if $type == $report.type}selected="selected"{/if}>{__("customer_orders_report.$type")}</option>
                {/foreach}
            </select>
        </div>

        {include file="common/period_selector.tpl" period=$search.period form_name="report_params_form"}
    </div>
    <hr>
    <div class="buttons-container ty-search-form__buttons-container">
        {include file="buttons/button.tpl" but_meta="ty-btn__secondary" but_text=__("customer_orders_report.create") but_name="dispatch[orders_report.view]"}
    </div>
</form>