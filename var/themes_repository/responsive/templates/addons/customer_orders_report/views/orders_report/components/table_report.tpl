<h3 class="ty-subheader">
    {__("customer_orders_report.spent_money")}
</h3>

<table class="ty-table">
    <thead>
    <tr>
        <th>{__("product")}</th>
        {foreach from=$report.period item="years" key="year"}
            {foreach from=$years item="months" key="month"}
                <th>{__("month_name_$month")} ({$year})</th>
            {/foreach}
        {/foreach}
    </tr>
    </thead>
    {foreach from=$report.data item="products" key="product"}
        <tr>
            <td>{$product}</td>
            {foreach from=$report.period item="years" key="year"}
                {foreach from=$years item="months" key="month"}
                    {$k = $year|cat:"$month"}
                    {if $products.$k}
                        <td>{include file="common/price.tpl" value=$products.$k}</td>
                    {else}
                        <td>-</td>
                    {/if}
                {/foreach}
            {/foreach}
        </tr>
    {/foreach}
</table>