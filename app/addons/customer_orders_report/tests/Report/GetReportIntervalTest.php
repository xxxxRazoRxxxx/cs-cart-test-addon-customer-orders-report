<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Tests\Unit\Addons\CustomerOrdersReport\Report;

use Tygh\Addons\CustomerOrdersReport\Report\SpentMoneyReport;
use Tygh\Tests\Unit\ATestCase;

class GetReportIntervalTest extends ATestCase
{
    /** @var SpentMoneyReport */
    protected $report;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->report = new SpentMoneyReport();
    }

    public function testGetReportInterval(){
        $params = [
            0   =>  [
                'time_to'   => 1575158400,
                'time_from' => 1568246400,
            ],
            1   =>  [
                'time_to'   => 1557619200,
                'time_from' => 1538352000,
            ],
            2   =>  [
                'time_to'   => 1575158400,
                'time_from' => '',
            ]
        ];

        $equals = [
            0   =>  [
                19 => [
                    9   => 199,
                    10  => 1910,
                    11  => 1911,
                    12  => 1912,
                ]
            ],
            1   =>  [
                18  =>  [
                    10  =>  1810,
                    11  =>  1811,
                    12  =>  1812,
                ],
                19  =>  [
                    1   =>  191,
                    2   =>  192,
                    3   =>  193,
                    4   =>  194,
                    5   =>  195,
                ]
            ]

        ];


        $this->assertEquals($equals[0], $this->report->getReportInterval($params[0]));
        $this->assertEquals($equals[1], $this->report->getReportInterval($params[1]));
        $this->assertFalse($this->report->getReportInterval($params[2]));
    }
}