<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Tests\Unit\Addons\CustomerOrdersReport\Service;

use Tygh\Addons\CustomerOrdersReport\Orders\OrderFactory;
use Tygh\Addons\CustomerOrdersReport\Report\SpentMoneyReport;
use Tygh\Addons\CustomerOrdersReport\Service;
use Tygh\Tests\Unit\ATestCase;

class OrderFactoryTest extends ATestCase
{
    /** @var OrderFactory */
    protected $order_factory;

    /** @var  \PHPUnit_Framework_MockObject_MockObject|\Tygh\Database\Connection */
    protected $db_connection;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->db_connection = $this->getMockBuilder(\Tygh\Database\Connection::class)
            ->setMethods(['error', 'getRow', 'query', 'getArray', 'hasError', 'getColumn', 'getSingleHash', 'getHash', 'getMultiHash'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->order_factory = new OrderFactory($this->db_connection);

        parent::setUp();
    }

    public function testGetOrders()
    {
        $this->db_connection->expects($this->once())->method('getArray')->with(
            'SELECT o.order_id, o.status, o.total, o.timestamp, od.data FROM ?:orders as o'
            . ' LEFT JOIN ?:order_data as od ON od.order_id = o.order_id'
            . ' WHERE o.user_id = 1'
            . ' AND od.type = ?s'
            . ' AND o.status IN (?a)'
            . ' ORDER BY o.timestamp ASC, o.order_id ASC',
            'G', ['P', 'C']
        );

        $params = [
            'user_id' => 1,
            'status' => ['P', 'C']
        ];

        $this->order_factory->getOrders($params);
    }

    public function testGetCategories()
    {
        $this->db_connection->expects($this->once())->method('getHash')->with(
            'SELECT cd.category, cd.category_id, pc.product_id FROM ?:category_descriptions as cd'
            . ' LEFT JOIN ?:products_categories as pc ON pc.category_id = cd.category_id'
            . ' WHERE pc.product_id in (?a)'
            . ' AND pc.link_type = ?s'
            . ' AND cd.lang_code = ?s',
            'product_id', [1, 2, 3, 4], 'M', 'ru'
        );

        $products_ids = [1, 2, 3, 4];

        $this->order_factory->getCategories($products_ids, 'ru');
    }
}