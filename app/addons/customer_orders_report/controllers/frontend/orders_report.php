<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Addons\CustomerOrdersReport\ServiceProvider;
use Tygh\Addons\CustomerOrdersReport\Report\Type\Type;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'view') {
    $params = $_REQUEST;

    $service = ServiceProvider::getService();

    $params['user_id'] = !empty($auth['user_id']) ? $auth['user_id'] : '';

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
    }

    if(!empty($params['user_id']) && !empty($params['type'])) {
        $params['status'] = ($params['type'] != 'general') ? array('P', 'C') : '';

        $report = $service->generateReport($params);

        Tygh::$app['view']->assign('report', array_merge(['type' => $params['type']], $report));
        Tygh::$app['view']->assign('search', $params);
    }

    Tygh::$app['view']->assign('report_types', Type::getReportTypes());
}
