<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport;


use Tygh\Addons\CustomerOrdersReport\Report\GeneralReport;
use Tygh\Addons\CustomerOrdersReport\Report\SpentMoneyReport;
use Tygh\Addons\CustomerOrdersReport\Report\TopCategoriesReport;
use Tygh\Addons\CustomerOrdersReport\Report\TopProductsReport;
use Tygh\Addons\CustomerOrdersReport\Report\Type\Type;

/**
 * Implements methods for working with a Customer OrderFactory Report
 *
 * @package Tygh\Addons\CustomerOrdersReport
 */
class Service
{
    /**
     * Generate report
     *
     * @param array     $params
     *
     * @return array
     */
    public function generateReport($params)
    {
        $report = [];

        if($params['type'] == Type::REPORT_TYPE_GENERAL) {
            $reporter = new GeneralReport();
            $report = $reporter->generate($params);
        } elseif ($params['type'] == Type::REPORT_TYPE_SPENT_MONEY) {
            $reporter = new SpentMoneyReport();
            $report = $reporter->generate($params);
        } elseif ($params['type'] == Type::REPORT_TYPE_TOP_PRODUCTS_REPORT) {
            $reporter = new TopProductsReport();
            $report = $reporter->generate($params);
        } elseif ($params['type'] == Type::REPORT_TYPE_TOP_CATEGORIES_REPORT) {
            $reporter = new TopCategoriesReport();
            $report = $reporter->generate($params);
        }

        return $report;
    }
}