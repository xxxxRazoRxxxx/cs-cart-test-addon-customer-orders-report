<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport\Report;


use Tygh\Addons\CustomerOrdersReport\ServiceProvider;

/**
 * The class provides methods to generate general customer report.
 *
 * @package Tygh\Addons\CustomerOrdersReport\Report
 */ 
class GeneralReport implements IReport
{
    /** @var array */
    protected $order_paid_statuses = ['P', 'C'];

    /**
     * Generate general report by params
     *
     * @param array     $params
     *
     * @return array
     */
    public function generate($params)
    {
        $order_list = ServiceProvider::getOrderFactory()->getOrdersWithProducts($params);

        $data = array(
            'paid_orders'           => 0,
            'paid_orders_total'     => 0,
            'paid_products'         => 0,
            'unpaid_orders'         => 0,
            'unpaid_orders_total'   => 0,
            'unpaid_products'       => 0,
        );

        foreach($order_list as $order) {

            if ($this->isPaidStatus($order['status'])) {
                $data['paid_orders']++;
                $data['paid_orders_total'] += $order['total'];
                foreach ($order['products'] as $product) {
                    $data['paid_products'] += $product['amount'];
                }
            }
            else {
                $data['unpaid_orders']++;
                $data['unpaid_orders_total'] += $order['total'];
                foreach ($order['products'] as $product) {
                    $data['unpaid_products'] += $product['amount'];
                }
            }
        }

        return array('data' => $data);
    }

    /**
     * Check if status is paid
     *
     * @param string     $order_status
     *
     * @return bool
     */
    private function isPaidStatus($order_status)
    {
        return in_array($order_status, $this->order_paid_statuses);
    }
}