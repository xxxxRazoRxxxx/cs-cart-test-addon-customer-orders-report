<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport\Report\Type;


/**
* Class ReportType
*
* @package Tygh\Addons\CustomerOrdersReport\Report\Type
*/
class Type
{
    /** General report */
    const REPORT_TYPE_GENERAL = 'general';

    /** Spent money report */
    const REPORT_TYPE_SPENT_MONEY = 'spent_money';

    /** Top products report */
    const REPORT_TYPE_TOP_PRODUCTS_REPORT = 'top_products';

    /** Top products report */
    const REPORT_TYPE_TOP_CATEGORIES_REPORT = 'top_categories';

    static public function getReportTypes()
    {
        return [
            Type::REPORT_TYPE_GENERAL,
            Type::REPORT_TYPE_SPENT_MONEY,
            Type::REPORT_TYPE_TOP_PRODUCTS_REPORT,
            Type::REPORT_TYPE_TOP_CATEGORIES_REPORT,
        ];
    }
}