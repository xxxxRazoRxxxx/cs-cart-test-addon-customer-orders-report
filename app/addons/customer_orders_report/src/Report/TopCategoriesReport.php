<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport\Report;


use Tygh\Addons\CustomerOrdersReport\ServiceProvider;

/**
 * The class provides methods to generate report of top 10 categories of bought products.
 *
 * @package Tygh\Addons\CustomerOrdersReport\Report
 */  
class TopCategoriesReport implements IReport
{
    /**
     * Generate report of top 10 categories by params
     *
     * @param array     $params
     *
     * @return array
     */
    public function generate($params)
    {
        $data = [];

        $orders_list = ServiceProvider::getOrderFactory()->getOrdersWithCategories($params);

        foreach ($orders_list as $order) {
            foreach ($order['products'] as $product) {
                $data[$product['category']['id']] = [
                    'id' => $product['category']['id'],
                    'name' => $product['category']['name'],
                    'count' => isset($data[$product['category']['id']]) ? $data[$product['category']['id']]['count'] + 1 : 1
                ];
            }
        }

        $data = fn_sort_array_by_key($data, 'count', SORT_DESC);
        $report['data'] = array_slice($data, 0, 10);

        return $report;
    }
}