<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport\Report;


use Tygh\Addons\CustomerOrdersReport\ServiceProvider;

/**
 * The class provides methods to generate report of customer spent money.
 *
 * @package Tygh\Addons\CustomerOrdersReport\Report
 */
class SpentMoneyReport implements IReport
{
    /**
     * Generate spent money report by params
     *
     * @param array     $params
     *
     * @return array
     */
    public function generate($params)
    {
        $data = [];

        $order_list = ServiceProvider::getOrderFactory()->getOrdersWithProducts($params);

        $period = $this->getReportInterval($params);

        foreach ($order_list as $order) {
            $year = date('y',$order['timestamp']);
            $month = date('n',$order['timestamp']);
            $period[$year][$month] = $year.$month;

            foreach ($order['products'] as $product) {
                $total = isset($data[$period[$year][$month]][$product['product_id']]['total']) ?  $data[$period[$year][$month]][$product['product_id']]['total'] : 0;
                $total = $total + $product['price'] * $product['amount'];
                $data[$product['product']][$period[$year][$month]] = $total;
            }
        }

        return [
            'period'    => $period,
            'data'      => $data,
        ];
    }

    /**
     * Get report interval by params
     *
     * @param array     $params
     *
     * @return array
     */
    public function getReportInterval($params)
    {
        if(empty($params['time_from']) || empty($params['time_to'])) {
            return false;
        }

        $period = [];

        $from_year = date('y', $params['time_from']);
        $from_month =  date('n', $params['time_from']);
        $to_year = date('y', $params['time_to']);
        $to_month = date('n', $params['time_to']);

        for ($i = $from_year; $i <= $to_year; $i++) {
            $_from_month = ($i > $from_year) ? 1 : $from_month;
            $month = ($i < $to_year) ? 12 : $to_month;

            for ($j = $_from_month; $j <= $month; $j++) {
                $period[$i][$j] = $i.$j;
            }
        }

        return $period;
    }
}