<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\CustomerOrdersReport\Orders;


use Tygh\Database\Connection;

class OrderFactory
{
    /** @var \Tygh\Database\Connection  */
    protected $db_connection;

    /**
     * OrderFactory constructor.
     *
     * @param \Tygh\Database\Connection $db_connection
     */
    public function __construct(Connection $db_connection)
    {
        $this->db_connection = $db_connection;
    }

    /**
     * Get orders with product groups
     *
     * @param array     $params
     *
     * @return array
     */
    public function getOrdersWithProducts($params)
    {
        $order_list = $this->getOrders($params);
        foreach($order_list as $key => $order) {
            $data = unserialize($order['data']);
            $order_list[$key]['products'] = $data[0]['products'];
        }

        return $order_list;
    }

    /**
     * Get orders with product groups and categories name
     *
     * @param array     $params
     *
     * @return array
     */
    public function getOrdersWithCategories($params)
    {
        $order_list = $this->getOrdersWithProducts($params);

        $product_ids = $this->getProductIds($order_list);

        $category_list = $this->getCategories($product_ids);

        foreach ($order_list as $order_key => $order) {
            foreach ($order['products'] as $product_key => $product) {
                $order_list[$order_key]['products'][$product_key]['category'] = [
                    'name' => $category_list[$product['product_id']]['category'],
                    'id' => $category_list[$product['product_id']]['category_id']
                ];
            }
        }

        return $order_list;
    }

    /**
     * Get orders by params
     *
     * @param array     $params
     *
     * @return array
     */
    public function getOrders($params)
    {
        $fields = [
            'o.order_id',
            'o.status',
            'o.total',
            'o.timestamp',
            'od.data'
        ];

        $join = 'LEFT JOIN ?:order_data as od ON od.order_id = o.order_id';

        $condition[] = sprintf('o.user_id = %s', $params['user_id']);
        $condition[] = sprintf('od.type = ?s');
        $args[] = 'G';

        if (!empty($params['status'])) {
            $condition[] = sprintf('o.status IN (%s)', '?a');
            $args[] = ['P', 'C'];
        }

        if (!empty($params['time_from']) && !empty($params['time_to']) ) {
            $condition[] = sprintf('(o.timestamp >= %s AND o.timestamp <= %s)', $params['time_from'], $params['time_to']);
        }

        $sorting = [
            'o.timestamp ASC',
            'o.order_id ASC'
        ];

        $query = sprintf(
            'SELECT %s FROM ?:orders as o %s WHERE %s ORDER BY %s',
            implode(', ', $fields),
            $join,
            implode(' AND ', $condition),
            implode(', ', $sorting)
        );

        return $this->db_connection->getArray($query, ...$args);
    }

    /**
     * Get categories name from product main category
     *
     * @param array     $params
     *
     * @return array
     */
    public function getCategories($product_ids, $lang_code = CART_LANGUAGE)
    {
        $fields = [
            'cd.category',
            'cd.category_id',
            'pc.product_id'
        ];

        $from = 'category_descriptions as cd';
        $join = 'LEFT JOIN ?:products_categories as pc ON pc.category_id = cd.category_id';

        $condition[] = sprintf('pc.product_id in (?a)');
        $args[] = $product_ids;

        $condition[] = sprintf('pc.link_type = ?s');
        $args[] = 'M';

        $condition[] = sprintf('cd.lang_code = ?s');
        $args[] = $lang_code;

        $query = sprintf(
            'SELECT %s FROM ?:%s %s WHERE %s',
            implode(', ', $fields),
            $from,
            $join,
            implode(' AND ', $condition)
        );

        return $this->db_connection->getHash($query, 'product_id', ...$args);
    }

    /**
     * Get products id from order list
     *
     * @param array     $order_list
     *
     * @return array
     */
    private function getProductIds($order_list)
    {
        $product_ids = [];

        foreach($order_list as $order) {
            foreach($order['products'] as $product) {
                $product_ids[] = $product['product_id'];
            }
        }

        return $product_ids;
    }
}